from django.contrib.auth.password_validation import get_default_password_validators


def password_validate(password):
    validators = get_default_password_validators()
    for validator in validators:
        validator.validate(password)
