import os
from django.utils import timezone
from django.core.files.storage import FileSystemStorage as FileSystemStorageRaw


class FileSystemStorage(FileSystemStorageRaw):
    '''
    添加按照日期为文件夹进行存储的功能
    如：avatar/1.jpg,修改为：
    avatar/1999-01-01/1.jpg
    '''
    @staticmethod
    def upload_path_handler(name):
        dir_path, name = os.path.split(name)
        now_time = timezone.now().strftime('%Y-%m-%d')
        return os.path.join(dir_path, now_time, name)

    def save(self, name, content, max_length=None):
        name = self.upload_path_handler(name)
        return super().save(name, content, max_length)
