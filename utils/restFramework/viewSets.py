from rest_framework.viewsets import ModelViewSet
from .mixins import GetSerializerClassMixin, GetPermissionsMixin, DestroyModelMixin


class CustomerModelViewSet(
    GetSerializerClassMixin, GetPermissionsMixin,
    DestroyModelMixin, ModelViewSet
):
    pass
