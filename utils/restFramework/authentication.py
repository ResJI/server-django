from rest_framework.authentication import SessionAuthentication as SessionAuthenticationRaw


class SessionAuthentication(SessionAuthenticationRaw):
    def authenticate(self, request):
        """
        Returns a `User` if the request session currently has a logged in user.
        Otherwise returns `None`.
        覆盖原有的authenticate，去除强制的enforce_csrf验证
        """

        # Get the session-based user from the underlying HttpRequest object
        user = getattr(request._request, 'user', None)

        # Unauthenticated, CSRF validation not required
        if not user or not user.is_active:
            return None

        return (user, None)
