# class IsAdminOrIsSelf()
from rest_framework.permissions import (
    BasePermission, OperandHolder, SAFE_METHODS, OperationHolderMixin
)


class IsOwner(OperationHolderMixin, BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.id == request.user.id


class ReadOnly(OperationHolderMixin, BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True


class IsAdmin(OperationHolderMixin, BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_staff)

    def has_object_permission(self, request, view, obj):
        return bool(request.user and request.user.is_staff)





