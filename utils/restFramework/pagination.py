from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination as PageNumberPaginationRaw
from rest_framework.response import Response


class PageNumberPagination(PageNumberPaginationRaw):
    page_size = 3                        # 配置单页个数
    max_page_size = 1000                 # 配置单页查询最大个数
    page_query_param = 'page_num'        # 配置查询页数字符串
    page_size_query_param = 'page_size'  # 配置查询个数字符串, 如'page_size'
    last_page_strings = ('last',)        # 配置最后一页快速查询字符串

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page_nums', self.page.paginator.num_pages),
            ('current_page', self.page.number),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
