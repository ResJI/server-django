from threading import Thread
import logging
from django.core.mail import send_mail

logger = logging.getLogger('django.request')


def async_send_mail(subject, message, from_email, recipient_list,
                    fail_silently=False, auth_user=None, auth_password=None,
                    connection=None, html_message=None):
    try:
        Thread(target=send_mail, args=(
            subject, message, from_email, recipient_list,
            fail_silently, auth_user, auth_password,
            connection, html_message
        )).start()
    except Exception as e:
        logger.error(str(e))
