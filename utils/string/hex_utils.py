import re


def hex_str_to_bytes(string):
    # clean_data = re.sub(r'[^0-9a-f]*', '', string, flags=re.IGNORECASE)
    # if len(clean_data) % 2 == 1:
    #     convert_list = list(clean_data)
    #     convert_list.insert(-1, '0')
    #     clean_data = ''.join(convert_list)
    # return bytearray.fromhex(clean_data)
    data_list = []
    clean_data = re.sub(r'[^0-9a-f]*', '', string, flags=re.IGNORECASE)
    for i in range(0, len(clean_data), 2):
        data_list.append(int(clean_data[i:i+2], 16))
    return bytearray(data_list)


def bytes_to_hex_str(bytes_data):
    value_list = []
    for byte in bytes_data:
        value_list.append('%02x' % int(byte))
    return ''.join(value_list)


if __name__ == '__main__':
    data = hex_str_to_bytes('01 0cc2f fbdn f')
    print(data)
    data = bytes_to_hex_str(data)
    print(data)

