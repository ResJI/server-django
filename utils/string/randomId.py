import random
import string
import time
import uuid as uuid_raw
from hashlib import sha256


def generate_id(length):
    """
    生成随机字符串，包含数字和字母
    """
    c_time = time.time()
    c_random = random.random()
    sha = sha256()
    sha.update(str(c_time).encode('utf-8'))
    sha.update(str(c_random).encode('utf-8'))
    random_id = sha.hexdigest()[:length]
    return random_id


def generate_num(length):
    """
    生成随机数字的字符串
    """
    return ''.join(random.sample(string.digits, length))


def generate_uuid():
    return uuid_raw.uuid4()


CHAR_SET = ("a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z")


def uuid():
    """
    返回一个UUID对象
    :return:
    """
    return uuid_raw.uuid4()


def uuid_36():
    """
    返回36字符的UUID字符串(十六进制,含有-)  bc5debab-95c3-4430-933f-2e3b6407ac30
    :return:
    """
    return str(uuid_raw.uuid4())


def uuid_32():
    """
    返回32字符的UUID字符串(十六进制)  bc5debab95c34430933f2e3b6407ac30
    :return:
    """
    return uuid_36().replace('-', '')


def uuid_8():
    """
    返回8字符的UUID字符串(非进制)  3FNWjtlD
    :return:
    """
    s = uuid_32()
    result = ''
    for i in range(0, 8):
        sub = s[i * 4: i * 4 + 4]
        x = int(sub, 16)
        result += CHAR_SET[x % 0x3E]
    return result


def uuid_16():
    """
    返回16字符的UUID字符串(非进制)  3FNWjtlD3FNWjtlD
    :return:
    """
    return uuid_8() + uuid_8()
