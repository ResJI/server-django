from django.urls import path, include, re_path
from rest_framework.routers import SimpleRouter
from apps.user.views import (
    Login, Logout, UserViewSet, GetSelfInfo,
    ResetSelfPassword, ResetOtherPassword
)

# Create a router and register our viewsets with it.
# 参考: https://q1mi.github.io/Django-REST-framework-documentation/api-guide/routers_zh/
router = SimpleRouter()
router.register(r'users', UserViewSet, basename='user')

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
    path('security/set-self-password/', ResetSelfPassword.as_view(), name='reset_self_password'),
    path('security/set-other-password/', ResetOtherPassword.as_view(), name='reset_other_password'),
    path('get-self-info/', GetSelfInfo.as_view(), name='getSelfInfo'),
    path('', include(router.urls)),  # 添加RESTframework的路由地址
]