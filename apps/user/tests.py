from django.test import TestCase

# Create your tests here.
import time
from .models import User

s_time = time.time()
for i in range(300):
    user = User(username='testUser_%d' % (i+1))
    user.set_password('baihuaiyu')
    user.is_active = True
    user.is_staff = True
    user.is_superuser = True
    user.save()
e_time = time.time()
print('用户批量生成完成,耗时:%.2fs' % (e_time-s_time))
