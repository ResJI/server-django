import uuid
from django.template import loader
from apps.user.models import Inactive
from utils.email import async_send_mail


def send_activate_mail(user):
    activate_key = str(uuid.uuid4())
    Inactive.objects.create(activate_key, user)
    html_message = loader.render_to_string('emails/activate_email.html', dict(activate_key=activate_key))
    async_send_mail(
        subject='账号激活',
        message='信息',
        from_email=None,
        recipient_list=[user.email],
        fail_silently=True,
        html_message=html_message
    )
