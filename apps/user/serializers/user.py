import os

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.cache import cache
from django.template import loader
from rest_framework import serializers

from utils.email import async_send_mail
from utils.string.randomId import generate_uuid, generate_num
from .check import CheckUsernameSerializer, CheckEmailSerializer, CheckPasswordSerializer

User = get_user_model()
USERNAME_FIELD = User.USERNAME_FIELD


class UserSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = User
        fields = (
            'url', 'id', 'username', 'nickname', 'real_name',
            'gender', 'email', 'avatar', 'date_joined', 'last_login'
        )
        # fields = '__all__'
        # exclude = (
        #     'password', 'create_datetime', 'is_staff', 'is_active', 'is_deleted',
        #     'user_permissions', 'groups', 'description', 'is_superuser', 'creator'
        # )
        extra_kwargs = {
            'username': {'required': False},
            'url': {'view_name': 'user-detail'},  # 用于配置反向查询URL的路径名，默认为'{model_name}-detail'
            'date_joined': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'last_login': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'update_datetime': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'create_datetime': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
        }
        depth = 1

    def update(self, instance, validated_data):
        old_avatar_name = instance.avatar.name
        old_avatar_path = instance.avatar.path
        instance = super().update(instance, validated_data)

        # 清除旧版头像
        if old_avatar_name and old_avatar_name != settings.DEFAULT_AVATAR:
            try:
                os.remove(old_avatar_path)
            except FileNotFoundError:
                pass

        return instance


class RegisterCaptchaSerializer(serializers.Serializer):
    email = serializers.EmailField(required=False)

    def validate_email(self, value):
        email_serializer = CheckEmailSerializer(data={'email': value})
        email_serializer.is_valid(raise_exception=True)
        return value

    def send_mail(self):
        captcha_key = generate_uuid()
        captcha_value = generate_num(6)
        cache.set(captcha_key, captcha_value, 5 * 6000)  # 将验证码保存至缓存中，5分钟后失效
        html_message = loader.render_to_string('emails/register_email.html', dict(captcha_value=captcha_value))
        async_send_mail(
            subject='注册验证码',
            message='信息',
            from_email=None,
            recipient_list=[self.validated_data['email']],
            fail_silently=True,
            html_message=html_message
        )
        return captcha_key


class CreateUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)
    if settings.REGISTER_EMAIL_CAPTCHA_ON:
        captcha_key = serializers.CharField(required=False, allow_blank=False)
        captcha_value = serializers.CharField(required=False, allow_blank=False)

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        extra_kwargs = {
            'username': {'required': True, 'allow_blank': True},
            'password': {'required': True, 'allow_blank': False, 'style': {'input_type': 'password'}},
        }
        depth = 1

    def validate(self, attrs):
        username_serializer = CheckUsernameSerializer(data={'username': attrs['username']})
        email_serializer = CheckEmailSerializer(data={'email': attrs['email']})
        password_serializer = CheckPasswordSerializer(data={'password': attrs['password']})
        valid_serializers = [username_serializer, email_serializer, password_serializer]
        for valid_serializer in valid_serializers:
            valid_serializer.is_valid(raise_exception=True)
        if settings.REGISTER_EMAIL_CAPTCHA_ON:
            captcha_data = {
                'captcha_key': attrs.get('captcha_key', None),
                'captcha_value': attrs.get('captcha_value', None)
            }
            if not all(captcha_data.values()):
                raise serializers.ValidationError('必须包含 "captcha_key" 和 "captcha_value" 字段')
            captcha_real_value = cache.get(attrs['captcha_key'])
            if (not captcha_real_value) or (not captcha_real_value == attrs['captcha_value']):
                raise serializers.ValidationError('验证码输入有误')
            cache.delete(attrs['captcha_key'])  # 删除缓存
        return attrs

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class UpdateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        # exclude = (
        #     'password', 'create_datetime', 'is_staff', 'is_active', 'is_deleted',
        #     'user_permissions', 'groups', 'description', 'is_superuser', 'creator'
        # )
        extra_kwargs = {
            'url': {'view_name': 'user-detail'},  # 用于配置反向查询URL的路径名，默认为'{model_name}-detail'
            'date_joined': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'last_login': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'update_datetime': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
            'create_datetime': {'read_only': True, 'format': "%Y-%m-%d %H:%M:%S"},
        }
        depth = 1

# class AvatarSerializer(serializers.Serializer):
#     avatar = serializers.ImageField()
