from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.contrib.auth.password_validation import validate_password

User = get_user_model()
USERNAME_FIELD = User.USERNAME_FIELD


class CheckUsernameSerializer(serializers.Serializer):
    username = serializers.CharField(min_length=3)

    def validate_username(self, value):
        kwargs = {USERNAME_FIELD: value}
        user = User.objects.all().filter(**kwargs).first()
        if user:
            raise serializers.ValidationError("用户名已存在")


class CheckPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=32)

    def validate_password(self, value):
        validate_password(value)


class CheckEmailSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def validate_email(self, value):
        user = User.objects.all().filter(email=value).first()
        if user:
            raise serializers.ValidationError("邮箱已存在")


# django.contrib.auth.password_validation.UserAttributeSimilarityValidator