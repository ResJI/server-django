from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    username_add = forms.CharField(label='用户名ID', help_text='小于150个字符，只能使用字母和@/./+/-/_',)
    password1 = forms.CharField(label='密码', widget=forms.PasswordInput)
    password2 = forms.CharField(label='密码确认', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ()

    def clean_username_add(self):
        username_add = self.cleaned_data.get("username_add")
        if not username_add:
            raise ValidationError("用户名不能为空")
        return username_add

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("两次密码输入不一致")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.username = self.cleaned_data['username_add']
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        # fields = ('username', 'password', 'date_of_birth', 'is_active', 'is_staff')
        fields = ('password', )

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    # form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on user.User.
    readonly_fields = ('username', 'date_joined')
    list_display = ('username', 'nickname', 'real_name')
    list_filter = ('is_staff',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('个人信息', {
            'fields': ('nickname', 'real_name', 'gender', 'avatar', 'email', 'date_of_birth', 'date_joined')
        }),
        # ('权限管理', {'fields': ('is_staff', 'is_active', 'groups', 'user_permissions')})
        ('权限管理', {'fields': ('is_staff', 'is_active', 'user_permissions')})
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username_add', 'password1', 'password2'),
        }),
        ('个人信息', {
            'fields': ('nickname', 'real_name', 'gender', 'avatar', 'email', 'date_of_birth', 'date_joined')
        }),
        # ('权限管理', {'fields': ('is_staff', 'is_active', 'groups', 'user_permissions')})
        ('权限管理', {'fields': ('is_staff', 'is_active', 'user_permissions')})
    )
    search_fields = ('email',)
    ordering = ('email',)
    # filter_horizontal = ('groups', 'user_permissions')  # 管理界面中添加多对多关系表添加
    filter_horizontal = ('user_permissions',)  # 管理界面中添加多对多关系表添加


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
# admin.site.unregister(Group)
