from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response

from apps.user.serializers import ResetSelfPasswordSerializer, ResetOtherPasswordSerializer
from utils.restFramework.permissions import (
    IsOwner
)
from rest_framework.permissions import IsAuthenticated


class ResetSelfPassword(GenericAPIView):
    serializer_class = ResetSelfPasswordSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({
            'data': '密码修改成功'
        })


class ResetOtherPassword(GenericAPIView):
    serializer_class = ResetOtherPasswordSerializer
    permission_classes = [IsAuthenticated & IsAdminUser]

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({
            'data': '密码修改成功'
        })
