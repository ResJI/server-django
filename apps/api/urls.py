from collections import OrderedDict
from django.urls import path, include, re_path
from rest_framework.routers import APIRootView
# from apps.system.views import SystemInfo
from .views import CaptchaView, upload
from utils.simple_JWT.views import TokenRefreshView
from apps.user.urls import urlpatterns as user_urls

# 浏览器中输入api跟目录时显示可用url
api_root_dict = OrderedDict({
    'captcha_generate': 'captcha_generate',
    'login': 'login',
    'logout': 'logout',
    'users': 'user-list',
})
api_root_view = APIRootView.as_view(api_root_dict=api_root_dict)

captcha_urls = [
    path('captcha_generate/', CaptchaView.as_view(), name='captcha_generate'),
    path('captcha/', include('captcha.urls')),
]

# The API URLs are now determined automatically by the router.
basic_urls = [
    path('', api_root_view),  # 设置api跟目录显示的url
    re_path(r'^restframework-auth/', include('rest_framework.urls', namespace='rest_framework')),  # 浏览器访问api时，添加登录功能，路由地址可自定义
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('upload/', upload),
    # path('system/info/', SystemInfo.as_view(), name='systemInfo')
]

urlpatterns = basic_urls+captcha_urls+user_urls

# print(urlpatterns)
