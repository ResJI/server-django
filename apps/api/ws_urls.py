from django.urls import path, re_path
from apps.workstation.webSocket import WebSocket
from apps.server_monitor.consumers import ServerMonitorWebsocket


websocket_urlpatterns = [
    path('workstation/', WebSocket.as_asgi()),
    path('server-monitor/', ServerMonitorWebsocket.as_asgi())
]
