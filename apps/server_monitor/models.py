from django.db import models


class SystemStatus(models.Model):
    cpu_num = models.CharField(max_length=8, verbose_name='CPU核数')
    cpu_utilization = models.CharField(max_length=8, verbose_name='CPU已使用率')
    mem_total = models.CharField(max_length=32, verbose_name='内存总数(MB)')
    mem_used = models.CharField(max_length=32, verbose_name='内存已使用大小(MB)')
    time = models.TimeField(auto_now=True)

    class Meta:
        verbose_name = '服务器监控信息'
        verbose_name_plural = verbose_name
