from django.apps import AppConfig


class ServerMonitorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.server_monitor'
    verbose_name = '服务器状态监控系统'
