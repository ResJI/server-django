# Generated by Django 3.2.10 on 2021-12-27 02:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('server_monitor', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='monitor',
            old_name='cpu_sys',
            new_name='cpu_utilization',
        ),
        migrations.RenameField(
            model_name='monitor',
            old_name='mem_num',
            new_name='mem_total',
        ),
        migrations.RenameField(
            model_name='monitor',
            old_name='mem_sys',
            new_name='mem_used',
        ),
        migrations.RemoveField(
            model_name='monitor',
            name='seconds',
        ),
    ]
