import time

from celery import shared_task


def get_time_str():
    time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    time_sec = time.time()
    time_ms = int((time_sec - int(time_sec)) * 1000)
    time_str = '%s.%03d' % (time_str, time_ms)
    return time_str


@shared_task
def monitor_device_task(ip):
    with open('taskFile.txt', 'a+') as f:
        time_str = get_time_str()
        f.writelines('%s: monitor device task: %s \n' % (time_str, ip))
