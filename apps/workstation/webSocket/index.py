from utils.channels import WebSocketTransfer
from .event_list import event_list


class WebSocket(WebSocketTransfer):
    event_list = event_list
