"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.staticfiles.views import serve as static_views_server
from django.shortcuts import reverse, redirect
from django.urls import path, re_path, include
from django.views.static import serve as static_server

from apps.workstation import urls as workstation_urls
from apps.api import urls as api_urls
from server.settings import STATIC_URL, MEDIA_URL, MEDIA_ROOT
from apps.index.views import index



# 静态资源和媒体资源路由
resource_urlpatterns = [
    # 该配置可实现静态文件的自动搜索，按照 STATICFILES_DIRS 设置的路径进行静态文件搜索
    re_path(r'^%s/(?P<path>.*)$' % STATIC_URL.replace('/', ''), static_views_server, kwargs={'insecure': True}),
    # 该配置可实现静态文件的服务，类似于生产环境的静态文件配置，需用 collectstatic 命令将静态文件打包至相应目录
    # re_path(r'^%s/(?P<path>.*)$' % STATIC_URL.replace('/', ''), static_server, kwargs={'document_root': STATIC_ROOT}),
    re_path(r'^%s/(?P<path>.*)$' % MEDIA_URL.replace('/', ''), static_server, kwargs={'document_root': MEDIA_ROOT}),
    path('favicon.ico', lambda request: redirect('%sfavicon.ico' % STATIC_URL)),
]

# 基础路由
base_urlpatterns = [
    path('', index),
    path('admin/', admin.site.urls),
    path('api/', include(api_urls)),
    # path('', lambda request: redirect(reverse('workstation:index'))),
]

urlpatterns = resource_urlpatterns + base_urlpatterns
