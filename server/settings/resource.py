import os
from .basic import BASE_DIR

# 静态资源配置
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static')
]
STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')
DEFAULT_AVATAR = 'avatar/default.jpg'  # 默认头像路径（该参数为自定义）

# 媒体资源配置
DEFAULT_FILE_STORAGE = 'utils.django.storage.FileSystemStorage'  # 默认文件存储器
MEDIA_URL = '/media/'  # 配置媒体资源的URL路径，若不设置该字段，上传文件默认路径为BASE_DIR
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')  # 配置上传的媒体文件保存路径
if not os.path.exists(MEDIA_ROOT):
    os.makedirs(MEDIA_ROOT)

# 文件上传处理配置
FILE_UPLOAD_HANDLERS = [  # 文件上传处理器
    "django.core.files.uploadhandler.MemoryFileUploadHandler",  # 内存文件上传处理器(当上传文件小于2.5M时，文件缓存至内存)
    "django.core.files.uploadhandler.TemporaryFileUploadHandler"  # 缓存文件上传处理器(当上传文件小于2.5M时，文件缓存至硬盘)
]
# 大文件上传缓存配置“TemporaryFileUploadHandler"文件上传处理器的子配置项
FILE_UPLOAD_TEMP_DIR = os.path.join(BASE_DIR, 'uploadCache')  # 上传大文件缓存目录，当文件小于2.5M时默认采用内存缓存
if not os.path.exists(FILE_UPLOAD_TEMP_DIR):
    os.makedirs(FILE_UPLOAD_TEMP_DIR)
