# 认证配置
AUTH_USER_MODEL = 'user.User'  # 自定义用户模型类，替代Django内认证系统自带的User(格式为’<app的label名>.<模型名>‘，注意不是路径)
AUTHENTICATION_BACKENDS = [  # 给Django或RESTframework登录界面提供用户名、密码验证
    # 'apps.user.authentication.MyModelBackend',   # 用于认证服务器认证用的
    'django.contrib.auth.backends.ModelBackend'  # 用户本地账号认证的后端（如本地管理员）
]
# LOGIN_URL = '/user/login/'  # 登录验证失败时，跳转的登录URL路径
# LOGIN_URL = '/admin/'  # 登录验证失败时，跳转的登录URL路径