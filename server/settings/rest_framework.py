
# DjangoRESTframework配置
REST_FRAMEWORK = {
    'NON_FIELD_ERRORS_KEY': 'general',  # 序列化器通用化验证异常时，返回的键值django_migrations
    'EXCEPTION_HANDLER': 'utils.restFramework.exceptionHandler.exception_handler',  # 自定义异常处理器
    # 'DEFAULT_PAGINATION_CLASS': 'utils.restFramework.pagination.PageNumberPagination',  # 自定义分页器
    'DEFAULT_RENDERER_CLASSES': (  # 配置默认渲染器
        'utils.restFramework.JSONRenderer.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer'
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (  # 配置默认认证后端类,在Django的django.contrib.auth.middleware.AuthenticationMiddleware之后验证,并覆盖Django原生验证结果
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework.authentication.BasicAuthentication',  # 用户名密码登录认证, 如请求头携带 Authorization: Basic YWRtaW46YmFpaHVhaXl1
        'utils.restFramework.authentication.SessionAuthentication'
        # 'rest_framework.authentication.SessionAuthentication',  # session登录认证（强制开启CSRF）
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        # 'rest_framework.permissions.IsAuthenticated',  # 配置默认所有地址都需要登录后访问
    ),
}
