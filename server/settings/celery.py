from .basic import TIME_ZONE

# 时区配置
CELERY_TIMEZONE = TIME_ZONE

# 中间人配置
# 格式：redis://:password@hostname:port/db_number
# 默认使用的是 localhost 的 6379 端口中 0 数据库。（ Redis 默认有 16 个数据库）
CELERY_BROKER_URL = 'redis://localhost:6379/0'

# 结果数据库
# 如果您想保存任务执行返回结果保存到Redis，您需要进行以下配置
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

# 结果过期时间
CELERY_RESULT_EXPIRES = 30*60

# 可见性超时设置
# 可见性超时为将消息重新下发给另外一个程序之前等待确认的任务秒数
CELERY_BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}

# 任务和结果格式配置
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# 定时调度程序Beat设置
# Beat调度器设置
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'


