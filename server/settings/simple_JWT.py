from datetime import timedelta
from django.conf import settings

# djangorestframework_simplejwt配置
SIMPLE_JWT = {
    # 过期时间配置
    # 'ACCESS_TOKEN_LIFETIME': timedelta(minutes=1),  # 访问Token过期时间
    # 'REFRESH_TOKEN_LIFETIME': timedelta(days=1),  # 刷新Token过期时间
    'ACCESS_TOKEN_LIFETIME': timedelta(seconds=30),  # 访问Token过期时间
    'REFRESH_TOKEN_LIFETIME': timedelta(seconds=60),  # 刷新Token过期时间

    # 若为True，在使用REFRESH_TOKEN认证时，
    # 返回新的REFRESH_TOKEN，否则不返回REFRESH_TOKEN，认证后的REFRESH_TOKEN在有效期内可多次使用
    # 通常配合BLACKLIST使用，使之前生成的REFRESH_TOKEN失效（放入黑名单）
    'ROTATE_REFRESH_TOKENS': True,

    # 若为True，则在REFRESH_TOKEN使用后加入黑名单令其失效
    # 开启该功能需在INSTALLED_APPS中加入'rest_framework_simplejwt.token_blacklist'
    'BLACKLIST_AFTER_ROTATION': True,

    # 在登录时更新auth_user（也可以是自定义的）表中的last_login字段为当前时间
    'UPDATE_LAST_LOGIN': False,

    # 加密算法配置
    # (HMAC：'HS256', 'HS384', 'HS512') HMAC算法，对称加密算法，加密、解密需要用同一个秘钥
    # (RSA:'RS256', 'RS384', 'RS512'） RSA算法，非对称加密算法，用公钥和私钥进行加密和解密
    'ALGORITHM': 'HS256',  # 加密算法
    'SIGNING_KEY': settings.SECRET_KEY,  # 签名秘钥
    'VERIFYING_KEY': None,  # 公钥，当使用RSA算法时才用到
    'AUDIENCE': None,  # 表明接收者身份，包含在载荷中（目前用不到）
    'ISSUER': None,  # 颁发者，包含在载荷中（目前用不到）

    # 认证标头名称
    # 'HTTP_AUTHORIZATION' 代表 'Authorization'
    # 'HTTP_X_ACCESS_TOKEN' 代表 'X_Access_Token'
    'AUTH_HEADER_NAME': 'HTTP_AUTHORIZATION',
    # 认证标头类型，可以是元组表示多种类型，
    # 例如'Bearer'，代表Authorization: Bearer <token>
    'AUTH_HEADER_TYPES': ('Bearer',),

    'USER_ID_FIELD': 'id',  # 数据库中表明用户的字段，包含在载荷中
    'USER_ID_CLAIM': 'user_id',  # 声明代表用户的字段，包含在载荷中

    # 是否启用认证规则检查（登录时验证），False为关闭检查
    # 可传入一个可调用的方法进行用户认证，在用户登录验证后、将用户对象传入自动执行
    # 若认证规则方法返回值为True，在代表认证通过，否则认证失败
    # 该参数如不设置，默认检查用户is_active字段是否为True，True则认证通过
    'USER_AUTHENTICATION_RULE': False,

    # Token认证类，用户配置AccessToken和SlidingToken认证
    # 'rest_framework_simplejwt.tokens.AccessToken' 可对AccessToken类型的JWT进行认证
    # 'rest_framework_simplejwt.tokens.SlidingToken' 可对SlidingToken类型的JWT进行认证
    'AUTH_TOKEN_CLASSES': ('rest_framework_simplejwt.tokens.AccessToken',),
    # Token类型字段声明,该声明对应的值有：“access”, “sliding” 和 “refresh”
    'TOKEN_TYPE_CLAIM': 'token_type',

    'JTI_CLAIM': 'jti',  # Token id字段声明，token id 用于撤销黑名单

    'SLIDING_TOKEN_REFRESH_EXP_CLAIM': 'refresh_exp',  # 刷新过期时间字段声明
    'SLIDING_TOKEN_LIFETIME': timedelta(minutes=5),  # 滑动Token有效时间
    'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),  # 滑动Token刷新时间
}

# 如下配置为自定义配置
TOKEN_ACCESS_CLAIM = 'access_token'  # access_token 的字段声明
TOKEN_REFRESH_CLAIM = 'refresh_token'  # refresh_token 的字段声明

# from rest_framework_simplejwt.tokens import AccessToken
# from rest_framework_simplejwt.tokens import SlidingToken
# from rest_framework_simplejwt.views import TokenObtainPairView