from django.urls import re_path
from channels.routing import URLRouter
from apps.api.ws_urls import websocket_urlpatterns

websocket_urlpatterns = [
    re_path(r'api/', URLRouter(websocket_urlpatterns)),
]